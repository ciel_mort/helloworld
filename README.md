Vanya, special for you.

1) Создай локально папку для репозитория;
2) git init
3) git remote add origin <url>
4) git pull <ссылка на репозиторий>


Основные команды:
1) git status - отслеживание изменений файлов репозитория;
2) git add - добавление файла для коммита;
3) git commit -m "некоторый текст" - отправление коммита в репо;
4) git push origin master- заливаешь с локалки на репо;

Алгоритм: 1 -> 2 -> 3 -> 4

Обучающий материал: https://ru.atlassian.com/git/tutorials/setting-up-a-repository